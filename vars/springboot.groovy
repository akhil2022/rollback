def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body() 
    pipeline {
	    agent none
        stages {
  	        stage('Maven Install') {
    	        agent {
      	            docker {
        	            image 'maven:3.5.0'
                    }
                }
                steps {
      	            sh 'mvn clean install'
                }
            }
            stage('Docker Build') {
    	        agent any
                steps {
      	            sh 'docker build .'
                }
            }
        }
    }
}
